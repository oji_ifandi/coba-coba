<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function index()
    {
        $games = DB::table('game')->get();

        return view('pages.game.index', [
            'games' => $games
        ]);
    }

    public function create()
    {
        return view('pages.game.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:game',
            'developer' => 'required',
            'year' => 'required',
            'gameplay' => 'required'
        ]);

        $query = DB::table('game')->insert([
            "name" => $request["name"],
            "developer" => $request["developer"],
            "year" => $request["year"],
            "gameplay" => $request["gameplay"]
        ]);

        return redirect('/game');
    }

    public function show($game_id)
    {
        $game = DB::table('game')->where('id', $game_id)->first();
        $platforms = DB::table('platform')->where('game_id', $game_id)->get();

        return view('pages.game.show', [
            'game' => $game,
            'platforms' => $platforms
        ]);
    }

    public function edit($game_id)
    {
        $game = DB::table('game')->where('id', $game_id)->first();

        return view('pages.game.edit', [
            'game' => $game
        ]);
    }

    public function update($game_id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'developer' => 'required',
            'year' => 'required',
            'gameplay' => 'required'
        ]);

        $query = DB::table('game')
            ->where('id', $game_id)
            ->update([
                "name" => $request["name"],
                "developer" => $request["developer"],
                "year" => $request["year"],
                "gameplay" => $request["gameplay"]
            ]);

        return redirect('/game');
    }

    public function destroy($game_id)
    {
        $query = DB::table('game')->where('id', $game_id)->delete();

        return redirect('/game');
    }
}
