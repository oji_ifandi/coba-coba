@extends('layouts.master')

@section('header', 'Daftar Game')

@section('content')
<div class="card">
	<div class="card-body">
		<a href="/game/create" class="btn btn-success btn-sm mb-2">Tambah Data</a>
		<table class="table table-bordered">
			<thead>                  
				<tr>
					<th style="width: 5%">#</th>
					<th>Nama</th>
					<th>Developer</th>
					<th style="width: 10%">Tahun</th>
					<th style="width: 20%">Aksi</th>
				</tr>
			</thead>
			<tbody>
				@forelse ($games as $key => $game)
				<tr>
					<td>{{ $key+1 }}</td>
					<td>{{ $game->name }}</td>
					<td>{{ $game->developer }}</td>
					<td>{{ $game->year }}</td>
					<td>
						<a href="/game/{{ $game->id }}" class="btn btn-sm btn-info">Show</a>
						<a href="/game/{{ $game->id }}/edit" class="btn btn-sm btn-primary">Edit</a>
						<form onclick="return confirm('hapus data?')" action="/game/{{$game->id}}" method="POST" class="d-inline">
							@method('DELETE')
							@csrf
							<button type="submit" class="btn btn-danger btn-sm">Delete</button>
						</form>
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="5" class="text-center">Data Kosong</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
@endsection

@push('scripts')
<script>
	Swal.fire({
		title: "Berhasil!",
		text: "Memasangkan script sweet alert",
		icon: "success",
		confirmButtonText: "Cool",
	});
</script>
@endpush