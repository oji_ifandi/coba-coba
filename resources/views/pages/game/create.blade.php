@extends('layouts.master')

@section('header', 'Tambah Data Game')

@section('content')
<div class="card">
	<div class="card-body">
		<form action="/game" method="post">
			@csrf
			<div class="form-group">
				<label>Nama</label>
				<input type="text" class="form-control" name="name">
				@error('name')
				<small class="text-danger">{{ $message }}</small>
				@enderror
			</div>
			<div class="form-group">
				<label>Developer</label>
				<input type="text" class="form-control" name="developer">
				@error('developer')
				<small class="text-danger">{{ $message }}</small>
				@enderror
			</div>
			<div class="form-group">
				<label>Tahun</label>
				<input type="number" class="form-control" name="year">
				@error('year')
				<small class="text-danger">{{ $message }}</small>
				@enderror
			</div>
			<div class="form-group">
				<label>Gameplay</label>
				<textarea class="form-control" rows="3" name="gameplay"></textarea>
				@error('gameplay')
				<small class="text-danger">{{ $message }}</small>
				@enderror
			</div>
			<button type="submit" class="btn btn-success mt-2">Simpan</button>
		</form>
	</div>
</div>
@endsection