@extends('layouts.master')

@section('header', 'Detail Game')

@section('content')
<div class="card">
	<div class="card-body">
		<h1 class="text-primary">{{ $game->name }}</h1>
		<span>Developer : {{ $game->developer }}</span>
		<h4 class="mt-4">Gameplay</h4>
		<p>{{ $game->gameplay }}</p>
		@foreach ($platforms as $platform)
			<span class="badge bg-primary ml-2 px-2 text-light">{{ $platform->name }}</span>
		@endforeach
	</div>
</div>
@endsection